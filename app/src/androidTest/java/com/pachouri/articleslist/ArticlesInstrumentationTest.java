package com.pachouri.articleslist;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.pachouri.articleslist.infrastructure.navigation.container.ToolbarFragmentContainerActivity;
import com.pachouri.articleslist.infrastructure.navigation.flow.FlowRoutes;
import com.pachouri.articleslist.main.activity.StartActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.pachouri.articleslist.infrastructure.navigation.container.BaseFragmentContainerActivity.BUNDLE_ROUTE;

/**
 * Created by Ankit on 11/18/18.
 */

@RunWith(AndroidJUnit4.class)
public class ArticlesInstrumentationTest {

    @Rule
    public ActivityTestRule<StartActivity> mStartActivityRule = new ActivityTestRule(StartActivity.class);

    /**
     * Test case: To test the started screen with start button is displayed and the button is clicked
     */
    @Test
    public void clickStartButton() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withId(R.id.buttonStart)).check(matches(isDisplayed()));
        onView(withId(R.id.buttonStart)).perform(click());
    }

    @Rule
    public ActivityTestRule<ToolbarFragmentContainerActivity> mSelectionScreenRule
            = new ActivityTestRule(ToolbarFragmentContainerActivity.class, false, false);

    /**
     * Test case: Launch new activity with intent value and check that the toolbar is displayed
     * and the toolbar title matches
     */
    @Test
    public void checkLaunchedActivity() {
        Intent i = new Intent();
        i.putExtra(BUNDLE_ROUTE, FlowRoutes.SelectionScreen.name());
        mSelectionScreenRule.launchActivity(i);
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onView(withId(R.id.textViewToolbarTitle)).check(matches(isDisplayed()));
        String toolbarTitle = mSelectionScreenRule.getActivity().getResources()
                .getString(R.string.title_selection_screen);
        onView(withText(toolbarTitle)).check(matches(isDisplayed()));
    }
}
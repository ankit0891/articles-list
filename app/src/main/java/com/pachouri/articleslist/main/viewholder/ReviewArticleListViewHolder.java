package com.pachouri.articleslist.main.viewholder;

import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.pachouri.articleslist.R;
import com.pachouri.articleslist.infrastructure.api.response.articles.Article_;
import com.pachouri.articleslist.main.adapter.ReviewArticleListAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ankit on 11/17/18.
 */
public class ReviewArticleListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.cardView)
    protected CardView mCardView;

    @BindView(R.id.imageViewItem)
    protected ImageView mImageViewItem;

    @BindView(R.id.imageViewLike)
    protected ImageView mImageViewLike;

    @BindView(R.id.linearLayoutTitle)
    protected LinearLayout mLinearLayoutTitle;

    @BindView(R.id.textViewTitle)
    protected TextView mTextViewTitle;

    public ReviewArticleListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bindView(Article_ article, ReviewArticleListAdapter.ListLayoutType layoutType) {
        Glide.with(mImageViewItem)
                .load(article.getMedia().get(0).getUri())
                .into(mImageViewItem);
        mTextViewTitle.setText(article.getTitle());
        if (layoutType == ReviewArticleListAdapter.ListLayoutType.List) {
            mLinearLayoutTitle.setVisibility(View.VISIBLE);
        } else {
            mLinearLayoutTitle.setVisibility(View.GONE);
        }
        if (article.isLiked()) {
            mImageViewLike.setVisibility(View.VISIBLE);
            mCardView.setForeground(new ColorDrawable(ContextCompat.getColor(itemView.getContext(), R.color.colorBlack_30)));
        } else {
            mImageViewLike.setVisibility(View.GONE);
            mCardView.setForeground(new ColorDrawable(ContextCompat.getColor(itemView.getContext(), android.R.color.transparent)));
        }
    }

    public static ReviewArticleListViewHolder create(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_review_article, parent, false);
        return new ReviewArticleListViewHolder(itemView);
    }
}

package com.pachouri.articleslist.main.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.pachouri.articleslist.R;
import com.pachouri.articleslist.infrastructure.api.response.articles.Article_;
import com.pachouri.articleslist.main.viewholder.ReviewArticleListViewHolder;
import com.pachouri.articleslist.util.HeaderFooterRecyclerViewAdapter;

import java.util.List;

/**
 * Created by Ankit on 11/17/18.
 */
public class ReviewArticleListAdapter extends HeaderFooterRecyclerViewAdapter<RecyclerView.ViewHolder, ReviewArticleListViewHolder, RecyclerView.ViewHolder> {

    public enum ListLayoutType {
        List, Grid;
    }

    private List<Article_> mList;
    private ListLayoutType mLayoutType;
    private Context mContext;
    private int mLastPosition = -1;

    public ReviewArticleListAdapter(Context context, List<Article_> list, ListLayoutType layoutType) {
        super(context);
        mList = list;
        mLayoutType = layoutType;
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    public ReviewArticleListViewHolder onCreateContentViewHolder(ViewGroup parent, int viewType) {
        return ReviewArticleListViewHolder.create(parent);
    }

    @Override
    public RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public void onBindContentViewHolder(ReviewArticleListViewHolder holder, int position) {
        Article_ article = mList.get(position);
        holder.bindView(article, mLayoutType);
        //Below code is used to animate the list item
        Animation animation = AnimationUtils.loadAnimation(mContext,
                (position > mLastPosition) ? R.anim.list_scroll_up
                        : R.anim.list_scroll_down);
        holder.itemView.startAnimation(animation);
        mLastPosition = position;
    }

    @Override
    public void onBindFooterViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getHeaderCount() {
        return 0;
    }

    @Override
    public int getContentCount() {
        return mList != null ? mList.size() : 0;
    }

    @Override
    public int getFooterCount() {
        return 0;
    }

    /**
     * Set List Layout Type
     *
     * @param layoutType
     */
    public void setLayoutType(ListLayoutType layoutType) {
        mLayoutType = layoutType;
    }
}

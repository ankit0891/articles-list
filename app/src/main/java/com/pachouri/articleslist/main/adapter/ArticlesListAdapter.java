package com.pachouri.articleslist.main.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ViewGroup;

import com.pachouri.articleslist.infrastructure.api.response.articles.Article_;
import com.pachouri.articleslist.main.viewholder.ArticleListViewHolder;
import com.pachouri.articleslist.main.viewholder.ArticleLoadMoreViewHolder;
import com.pachouri.articleslist.util.HeaderFooterRecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ankit on 11/15/18.
 */
public class ArticlesListAdapter extends HeaderFooterRecyclerViewAdapter<RecyclerView.ViewHolder, ArticleListViewHolder, ArticleLoadMoreViewHolder> {

    private List<Article_> mList;

    public ArticlesListAdapter(Context context) {
        super(context);
        mList = new ArrayList<>();
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    public ArticleListViewHolder onCreateContentViewHolder(ViewGroup parent, int viewType) {
        return ArticleListViewHolder.create(parent);
    }

    @Override
    public ArticleLoadMoreViewHolder onCreateFooterViewHolder(ViewGroup parent) {
        return ArticleLoadMoreViewHolder.create(parent);
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public void onBindContentViewHolder(ArticleListViewHolder holder, int position) {
        Article_ article = mList.get(position);
        holder.bindView(article);
    }

    @Override
    public void onBindFooterViewHolder(ArticleLoadMoreViewHolder holder, int position) {

    }

    @Override
    public int getHeaderCount() {
        return 0;
    }

    @Override
    public int getContentCount() {
        return mList != null ? mList.size() : 0;
    }

    @Override
    public int getFooterCount() {
        return 0;
    }

    /**
     * Set the List
     *
     * @param newList
     * @return
     */
    public ArticlesListAdapter setList(List<Article_> newList) {
        Log.v("ListSize", " : " + newList.size());
        mList.clear();
        mList.addAll(newList);
        notifyDataSetChanged();
        return this;
    }

    /**
     * Get the updated List
     *
     * @return
     */
    public List<Article_> getArticlesList() {
        return mList;
    }
}


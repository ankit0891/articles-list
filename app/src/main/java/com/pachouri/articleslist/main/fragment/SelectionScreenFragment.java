package com.pachouri.articleslist.main.fragment;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pachouri.articleslist.R;
import com.pachouri.articleslist.infrastructure.api.response.articles.Article_;
import com.pachouri.articleslist.infrastructure.navigation.Navigator;
import com.pachouri.articleslist.infrastructure.navigation.container.FragmentContainerSettings;
import com.pachouri.articleslist.main.adapter.ArticlesListAdapter;
import com.pachouri.articleslist.util.CommonUtils;
import com.pachouri.articleslist.viewmodel.ViewModelProvider;
import com.pachouri.articleslist.viewmodel.model.LoadingState;
import com.pachouri.articleslist.viewmodel.provider.ArticlesListViewModel;
import com.yuyakaido.android.cardstackview.CardStackLayoutManager;
import com.yuyakaido.android.cardstackview.CardStackListener;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.Direction;
import com.yuyakaido.android.cardstackview.StackFrom;
import com.yuyakaido.android.cardstackview.SwipeAnimationSetting;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ankit on 11/14/18.
 */
public class SelectionScreenFragment extends Fragment implements FragmentContainerSettings, CardStackListener {

    //Constant to Change the next api call
    private static final int LOAD_MORE_DIFFERENCE = 5;
    //Constant to change the enabling of Review button
    private static final int COUNT_REVIEW_ENABLE = 20;

    @BindView(R.id.cardStackView)
    protected CardStackView mCardStackView;

    @BindView(R.id.buttonReview)
    protected Button mButtonReview;

    @BindView(R.id.buttonLike)
    protected Button mButtonLike;

    @BindView(R.id.buttonDislike)
    protected Button mButtonDislike;

    @BindView(R.id.textViewCounter)
    protected TextView mTextViewCounter;

    @BindView(R.id.progressBar)
    protected ProgressBar mProgressBar;

    @BindView(R.id.layoutMainContainer)
    protected RelativeLayout mLayoutMainContainer;

    @BindView(R.id.layoutErrorContainer)
    protected LinearLayout mLayoutErrorContainer;

    @BindView(R.id.textViewErrorTitle)
    protected TextView mTextViewErrorTitle;

    @BindView(R.id.textViewErrorSubTitle)
    protected TextView mTextViewErrorSubTitle;

    @BindView(R.id.layoutNoMoreItems)
    protected LinearLayout mLayoutNoMoreItems;

    @BindView(R.id.imageViewBigHeart)
    protected ImageView mImageViewBigHeart;

    @BindView(R.id.imageViewSmallHeart)
    protected ImageView mImageViewSmallHeart;

    private ArticlesListViewModel mViewModel;
    private ArticlesListAdapter mAdapter;
    private CardStackLayoutManager mCardManager;
    private int mTotalCount;
    private int mLikeCount = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_selection_screen, container, false);
        ButterKnife.bind(this, view);
        mButtonReview.setEnabled(false);

        if (mAdapter == null) {
            mAdapter = new ArticlesListAdapter(getContext());
        }

        if (null == mViewModel) {
            mViewModel = ViewModelProvider.getInstance().getArticlesListViewModel(this);
        }

        mViewModel.getList().observe(this, mAdapter::setList);

        mViewModel.getCanLoadMoreItems().observe(this, hasMore -> {
            handleView(hasMore);
        });

        mViewModel.getInitialLoadState().observe(this, loadingState -> {
            if (loadingState == LoadingState.LOADING_COMPLETED) {
                showMainLayout();
            } else if (loadingState == LoadingState.LOADING_FAILED) {
                showSomethingWrongLayout();
            } else {
                showProgressBar();
            }
        });
        mViewModel.getTotalCount().observe(this, totalCount -> {
            mTotalCount = totalCount;
            mTextViewCounter.setText(getString(R.string.like_count, mLikeCount, mTotalCount));
        });
        setUpCardView();
        makeApiCall();
        return view;
    }

    /**
     * Method to make the initial Api call
     */
    private void makeApiCall() {
        if (CommonUtils.isInternetConnected(getContext())) {
            showProgressBar();
            mViewModel.refresh();
        } else {
            showNoInternetLayout();
        }
    }

    /**
     * Hide/Show UI when its a last item
     *
     * @param isVisible
     */
    private void handleView(boolean isVisible) {
        if (isVisible) {
            mButtonLike.setVisibility(View.VISIBLE);
            mButtonDislike.setVisibility(View.VISIBLE);
            mCardStackView.setVisibility(View.VISIBLE);
            mLayoutNoMoreItems.setVisibility(View.GONE);
        } else {
            mButtonLike.setVisibility(View.INVISIBLE);
            mButtonDislike.setVisibility(View.INVISIBLE);
            mCardStackView.setVisibility(View.GONE);
            mLayoutNoMoreItems.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Initialize the CardStackLayoutManager,
     * set the required behavior and set adapter to the CardStackView
     */
    private void setUpCardView() {
        mCardManager = new CardStackLayoutManager(getContext(), this);
        mCardManager.setStackFrom(StackFrom.None);
        mCardManager.setDirections(Direction.HORIZONTAL);
        mCardManager.setCanScrollHorizontal(true);
        mCardManager.setCanScrollVertical(true);
        mCardStackView.setLayoutManager(mCardManager);
        mCardStackView.setAdapter(mAdapter);
    }

    /**
     * When Like/Dislike button is clicked, perform swipe
     *
     * @param direction
     */
    private void swipeOnButtonsClick(Direction direction) {
        SwipeAnimationSetting setting = new SwipeAnimationSetting.Builder()
                .setDirection(direction)
                .setDuration(200)
                .setInterpolator(new AccelerateInterpolator())
                .build();
        mCardManager.setSwipeAnimationSetting(setting);
        mCardStackView.swipe();
    }

    /**
     * Method to show only progress bar
     */
    private void showProgressBar() {
        if (getActivity() != null && null != mProgressBar) {
            mProgressBar.setVisibility(View.VISIBLE);
            mLayoutMainContainer.setVisibility(View.GONE);
            mLayoutErrorContainer.setVisibility(View.GONE);
        }
    }

    /**
     * Method to show content layout
     */
    private void showMainLayout() {
        if (getActivity() != null && null != mProgressBar) {
            mProgressBar.setVisibility(View.GONE);
            mLayoutMainContainer.setVisibility(View.VISIBLE);
            mLayoutErrorContainer.setVisibility(View.GONE);
        }
    }

    /**
     * Method to show no internet available layout
     */
    private void showNoInternetLayout() {
        if (getActivity() != null && null != mProgressBar) {
            mProgressBar.setVisibility(View.GONE);
            mLayoutMainContainer.setVisibility(View.GONE);
            mLayoutErrorContainer.setVisibility(View.VISIBLE);
            mTextViewErrorTitle.setText(getString(R.string.error_oops));
            mTextViewErrorSubTitle.setText(getString(R.string.error_message_internet_not_found));
        }
    }

    /**
     * Method to show something went wrong layout
     */
    private void showSomethingWrongLayout() {
        if (getActivity() != null && null != mProgressBar) {
            mProgressBar.setVisibility(View.GONE);
            mLayoutMainContainer.setVisibility(View.GONE);
            mLayoutErrorContainer.setVisibility(View.VISIBLE);
            mTextViewErrorTitle.setText(getString(R.string.error_sorry));
            mTextViewErrorSubTitle.setText(getString(R.string.error_message_something_wrong));
        }
    }

    @OnClick(R.id.buttonDislike)
    protected void onDislikeButtonClicked() {
        swipeOnButtonsClick(Direction.Left);
    }

    @OnClick(R.id.buttonLike)
    protected void onLikeButtonClicked() {
        animateHeartLike(mImageViewBigHeart, mImageViewSmallHeart);
    }

    @OnClick(R.id.buttonReview)
    protected void onReviewButtonClicked() {
        startActivity(Navigator.getInstance().getReviewScreenIntent(getContext(), mAdapter.getArticlesList()));
        getActivity().overridePendingTransition(R.anim.enter_screen, R.anim.exit_screen);
    }

    @OnClick(R.id.buttonTryAgain)
    protected void onTryAgainButtonClicked() {
        makeApiCall();
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    @Override
    public boolean shouldShowBackButton() {
        return true;
    }

    @Override
    public boolean hasToolbar() {
        return true;
    }

    @Override
    public int getPageTitle() {
        return R.string.title_selection_screen;
    }

    @Override
    public void onCardDragging(Direction direction, float ratio) {
        //Empty, not to perform anything here at the moment
    }

    @Override
    public void onCardSwiped(Direction direction) {
        if (mCardManager != null && mAdapter != null && mViewModel != null) {
            if (mCardManager.getTopPosition() == mAdapter.getItemCount() - LOAD_MORE_DIFFERENCE) {
                mViewModel.loadMore();
            }
            if (mCardManager.getTopPosition() >= COUNT_REVIEW_ENABLE && !mButtonReview.isEnabled()) {
                mButtonReview.setEnabled(true);
                Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.button_bounce);
                mButtonReview.startAnimation(animation);
            }
            if (direction == Direction.Right) {
                Article_ article = mAdapter.getArticlesList().get(mCardManager.getTopPosition() - 1);
                article.setLiked(true);
                mLikeCount++;
                mTextViewCounter.setText(getString(R.string.like_count, mLikeCount, mTotalCount));
            }
        }
    }

    @Override
    public void onCardRewound() {
        //Empty, not to perform anything here at the moment
    }

    @Override
    public void onCardCanceled() {
        //Empty, not to perform anything here at the moment
    }

    /**
     * Heart Animation
     *
     * @param imageViewBigHeart
     * @param imageViewSmallHeart
     */
    private void animateHeartLike(final ImageView imageViewBigHeart, final ImageView imageViewSmallHeart) {
        Handler handler = new Handler();
        handler.post(() -> {
            imageViewBigHeart.setVisibility(View.VISIBLE);
            imageViewSmallHeart.setVisibility(View.GONE);
            imageViewBigHeart.setScaleY(0.1f);
            imageViewBigHeart.setScaleX(0.1f);
            imageViewBigHeart.setAlpha(1f);
            imageViewSmallHeart.setScaleY(0.1f);
            imageViewSmallHeart.setScaleX(0.1f);

            AnimatorSet animatorSet = new AnimatorSet();

            ObjectAnimator bgScaleYAnim = ObjectAnimator.ofFloat(imageViewSmallHeart, "scaleY", 0.1f, 1f);
            bgScaleYAnim.setDuration(300);
            bgScaleYAnim.setInterpolator(new DecelerateInterpolator());

            ObjectAnimator bgScaleXAnim = ObjectAnimator.ofFloat(imageViewSmallHeart, "scaleX", 0.1f, 1f);
            bgScaleXAnim.setDuration(300);
            bgScaleXAnim.setInterpolator(new DecelerateInterpolator());
            ObjectAnimator bgAlphaAnim = ObjectAnimator.ofFloat(imageViewSmallHeart, "alpha", 1f, 0f);
            bgAlphaAnim.setDuration(300);
            bgAlphaAnim.setStartDelay(150);
            bgAlphaAnim.setInterpolator(new DecelerateInterpolator());

            ObjectAnimator imgScaleUpYAnim = ObjectAnimator.ofFloat(imageViewBigHeart, "scaleY", 0.1f, 1f);
            imgScaleUpYAnim.setDuration(300);
            imgScaleUpYAnim.setInterpolator(new DecelerateInterpolator());

            ObjectAnimator imgScaleUpXAnim = ObjectAnimator.ofFloat(imageViewBigHeart, "scaleX", 0.1f, 1f);
            imgScaleUpXAnim.setDuration(300);
            imgScaleUpXAnim.setInterpolator(new DecelerateInterpolator());

            ObjectAnimator imgScaleDownYAnim = ObjectAnimator.ofFloat(imageViewBigHeart, "scaleY", 1f, 0f);
            imgScaleDownYAnim.setDuration(300);
            imgScaleDownYAnim.setInterpolator(new AccelerateInterpolator());
            ObjectAnimator imgScaleDownXAnim = ObjectAnimator.ofFloat(imageViewBigHeart, "scaleX", 1f, 0f);
            imgScaleDownXAnim.setDuration(300);
            imgScaleDownXAnim.setInterpolator(new AccelerateInterpolator());

            animatorSet.playTogether(bgScaleYAnim, bgScaleXAnim, bgAlphaAnim, imgScaleUpYAnim, imgScaleUpXAnim);
            animatorSet.play(imgScaleDownYAnim).with(imgScaleDownXAnim).after(imgScaleUpYAnim);

            animatorSet.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    imageViewBigHeart.setVisibility(View.GONE);
                    imageViewSmallHeart.setVisibility(View.GONE);
                    swipeOnButtonsClick(Direction.Right);
                }

                @Override
                public void onAnimationStart(Animator animation) {

                }
            });
            animatorSet.start();
        });
    }
}

package com.pachouri.articleslist.main.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.pachouri.articleslist.R;
import com.pachouri.articleslist.infrastructure.navigation.Navigator;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.buttonStart)
    protected void onStartButtonClicked() {
        startActivity(Navigator.getInstance().getSelectionScreenIntent(getApplicationContext()));
        overridePendingTransition(R.anim.enter_screen, R.anim.exit_screen);
    }
}

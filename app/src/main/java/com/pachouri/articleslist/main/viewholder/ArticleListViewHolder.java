package com.pachouri.articleslist.main.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.pachouri.articleslist.R;
import com.pachouri.articleslist.infrastructure.api.response.articles.Article_;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ankit on 11/15/18.
 */
public class ArticleListViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.imageViewItem)
    protected ImageView mImageViewItem;

    public ArticleListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bindView(Article_ article) {
        Glide.with(mImageViewItem)
                .load(article.getMedia().get(0).getUri())
                .into(mImageViewItem);

    }

    public static ArticleListViewHolder create(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_article, parent, false);
        return new ArticleListViewHolder(itemView);
    }
}

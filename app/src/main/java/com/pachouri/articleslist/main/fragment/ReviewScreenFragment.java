package com.pachouri.articleslist.main.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.pachouri.articleslist.R;
import com.pachouri.articleslist.infrastructure.api.response.articles.Article_;
import com.pachouri.articleslist.infrastructure.navigation.container.FragmentContainerSettings;
import com.pachouri.articleslist.main.adapter.ReviewArticleListAdapter;
import com.pachouri.articleslist.util.AppConstants;
import com.pachouri.articleslist.util.GridItemDecoration;
import com.pachouri.articleslist.util.ListItemDecoration;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Ankit on 11/17/18.
 */
public class ReviewScreenFragment extends Fragment implements FragmentContainerSettings {

    @BindView(R.id.recyclerView)
    protected RecyclerView mRecyclerView;

    @BindView(R.id.buttonList)
    protected Button mButtonList;

    @BindView(R.id.buttonGrid)
    protected Button mButtonGrid;

    private List<Article_> mList;

    private ReviewArticleListAdapter mAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private GridLayoutManager mGridLayoutManager;
    private ListItemDecoration mListItemDecoration;
    private GridItemDecoration mGridItemDecoration;

    public static Bundle createBundle(List<Article_> list) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.INTENT_ARTICLES_LIST, (Serializable) list);
        return bundle;
    }

    private void handleBundle() {
        if (getActivity() != null) {
            Bundle bundle = getActivity().getIntent().getBundleExtra(AppConstants.BUNDLE_ARTICLES_LIST);
            mList = (ArrayList<Article_>) bundle.getSerializable(AppConstants.INTENT_ARTICLES_LIST);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_review_screen, container, false);
        ButterKnife.bind(this, view);
        handleBundle();
        mButtonList.setSelected(true);
        setReviewedArticlesList();
        return view;
    }

    @OnClick(R.id.buttonList)
    protected void onButtonListClicked() {
        if (!mButtonList.isSelected()) toggleList(true);
    }

    @OnClick(R.id.buttonGrid)
    protected void onButtonGridClicked() {
        if (!mButtonGrid.isSelected()) toggleList(false);
    }

    /**
     * Initialise adapter, layoutManagers and itemDecorators,
     * <p>
     * and setting default ListLayoutType as List
     */
    private void setReviewedArticlesList() {
        mLinearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager
                .VERTICAL, false);
        mGridLayoutManager = new GridLayoutManager(getContext(), 2);
        mListItemDecoration = new ListItemDecoration(getContext(), 10f, 10f);
        mGridItemDecoration = new GridItemDecoration(getContext(), 2, 10f);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.addItemDecoration(mListItemDecoration);
        mAdapter = new ReviewArticleListAdapter(getContext(), mList, ReviewArticleListAdapter.ListLayoutType.List);
        mRecyclerView.setAdapter(mAdapter);
    }

    /**
     * Set the Linear List
     */
    private void setLinearList() {
        mRecyclerView.removeItemDecoration(mGridItemDecoration);
        mRecyclerView.addItemDecoration(mListItemDecoration);
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mAdapter.setLayoutType(ReviewArticleListAdapter.ListLayoutType.List);
    }

    /**
     * Set the Grid List
     */
    private void setGridList() {
        mRecyclerView.removeItemDecoration(mListItemDecoration);
        mRecyclerView.addItemDecoration(mGridItemDecoration);
        mRecyclerView.setLayoutManager(mGridLayoutManager);
        mAdapter.setLayoutType(ReviewArticleListAdapter.ListLayoutType.Grid);
    }

    /**
     * Toggle the list based on the flag
     *
     * @param isList
     */
    private void toggleList(boolean isList) {
        int scrollPosition = 0;
        if (isList) {
            mButtonList.setSelected(true);
            mButtonGrid.setSelected(false);
            scrollPosition = mGridLayoutManager.findFirstCompletelyVisibleItemPosition();
            setLinearList();
        } else {
            mButtonList.setSelected(false);
            mButtonGrid.setSelected(true);
            scrollPosition = mLinearLayoutManager.findFirstCompletelyVisibleItemPosition();
            setGridList();
        }
        mRecyclerView.scrollToPosition(scrollPosition);
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }

    @Override
    public boolean shouldShowBackButton() {
        return true;
    }

    @Override
    public boolean hasToolbar() {
        return true;
    }

    @Override
    public int getPageTitle() {
        return R.string.title_review_screen;
    }
}

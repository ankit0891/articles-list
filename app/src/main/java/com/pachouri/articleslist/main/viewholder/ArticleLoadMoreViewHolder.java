package com.pachouri.articleslist.main.viewholder;

import android.support.v7.widget.AppCompatTextView;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.pachouri.articleslist.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Ankit on 11/15/18.
 */
public class ArticleLoadMoreViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.textViewLoadMore)
    protected AppCompatTextView mTextViewLoadMore;

    public ArticleLoadMoreViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public static ArticleLoadMoreViewHolder create(ViewGroup parent) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_load_more_items, parent, false);
        return new ArticleLoadMoreViewHolder(itemView);
    }
}

package com.pachouri.articleslist.infrastructure.api.provider.retrofit2;

import com.pachouri.articleslist.infrastructure.api.ApiConstants;
import com.pachouri.articleslist.infrastructure.api.response.BaseResponse;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by Ankit on 11/14/18.
 */
public interface RetrofitInterface {

    @GET(ApiConstants.ENDPOINT_ARTICLES)
    Observable<BaseResponse> getArticlesList(@QueryMap Map<String, String> map);
}

package com.pachouri.articleslist.infrastructure.api.response.articles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Filter {

    @SerializedName("values")
    @Expose
    private List<Value> values = null;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("_metadata")
    @Expose
    private Metadata_ metadata;
    @SerializedName("min")
    @Expose
    private Integer min;
    @SerializedName("max")
    @Expose
    private Integer max;
    @SerializedName("currentMin")
    @Expose
    private Integer currentMin;
    @SerializedName("currentMax")
    @Expose
    private Integer currentMax;
    @SerializedName("unit")
    @Expose
    private String unit;

    public List<Value> getValues() {
        return values;
    }

    public String getId() {
        return id;
    }

    public Integer getPriority() {
        return priority;
    }

    public String getTitle() {
        return title;
    }

    public Metadata_ getMetadata() {
        return metadata;
    }

    public Integer getMin() {
        return min;
    }

    public Integer getMax() {
        return max;
    }

    public Integer getCurrentMin() {
        return currentMin;
    }

    public Integer getCurrentMax() {
        return currentMax;
    }

    public String getUnit() {
        return unit;
    }
}
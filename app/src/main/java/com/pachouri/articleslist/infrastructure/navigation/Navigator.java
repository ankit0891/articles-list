package com.pachouri.articleslist.infrastructure.navigation;

import com.pachouri.articleslist.infrastructure.navigation.flow.AppNavigator;
import com.pachouri.articleslist.infrastructure.navigation.flow.FlowNavigator;

/**
 * Get the instance of the AppNavigator class
 * <p>
 * Created by Ankit on 11/14/18.
 */
public class Navigator {

    private static volatile AppNavigator mInstance;

    public static synchronized AppNavigator getInstance() {
        if (mInstance == null) {
            synchronized (FlowNavigator.class) {
                if (mInstance == null) {
                    mInstance = new FlowNavigator();
                }
            }
        }
        return mInstance;
    }
}

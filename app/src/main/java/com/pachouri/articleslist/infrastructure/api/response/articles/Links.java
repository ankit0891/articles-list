package com.pachouri.articleslist.infrastructure.api.response.articles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Links {

    @SerializedName("self")
    @Expose
    private Self self;
    @SerializedName("articles")
    @Expose
    private List<Article> articles = null;
    @SerializedName("next")
    @Expose
    private Next next;

    public Self getSelf() {
        return self;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public Next getNext() {
        return next;
    }
}
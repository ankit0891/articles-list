package com.pachouri.articleslist.infrastructure.api.deserialzer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.reflect.TypeToken;
import com.pachouri.articleslist.infrastructure.api.GsonProvider;
import com.pachouri.articleslist.infrastructure.api.response.articles.Article_;
import com.pachouri.articleslist.infrastructure.api.response.articles.Embedded;

import java.lang.reflect.Type;

/**
 * Purpose of this class is to Deserialize the Embedded class as it has a generic member
 * <p>
 * Created by Ankit on 11/15/18.
 */
public class EmbeddedDeserializer implements JsonDeserializer<Embedded> {
    @Override
    public Embedded deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        Type collectionType = new TypeToken<Embedded<Article_>>() {
        }.getType();
        return GsonProvider.getBuilderExcluding(Embedded.class).create().fromJson(jsonObject, collectionType);
    }
}

package com.pachouri.articleslist.infrastructure.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pachouri.articleslist.infrastructure.api.deserialzer.EmbeddedDeserializer;
import com.pachouri.articleslist.infrastructure.api.response.articles.Embedded;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by Ankit on 11/15/18.
 */
public class GsonProvider {

    private static volatile Gson gson;

    public static synchronized Gson getSingletonGson() {
        if (gson == null) {
            synchronized (Gson.class) {
                if (gson == null) {
                    gson = getBuilder().create();
                }
            }
        }
        return gson;
    }

    public static GsonBuilder getBuilder() {
        return getBuilderExcludingTypes(null);
    }

    public static GsonBuilder getBuilderExcluding(Type types) {
        Set<Type> set = new HashSet<>();
        set.add(types);
        return getBuilderExcludingTypes(set);
    }

    private static GsonBuilder getBuilderExcludingTypes(Set<Type> types) {
        Map<Type, Object> deserializers = new HashMap<>();
        deserializers.put(Embedded.class, new EmbeddedDeserializer());
        if (types != null) {
            Iterator<Type> iterator = types.iterator();
            while (iterator.hasNext()) {
                deserializers.remove(iterator.next());
            }
        }
        GsonBuilder builder = new GsonBuilder();
        Iterator<Type> deserializerTypesIterator = deserializers.keySet().iterator();
        while (deserializerTypesIterator.hasNext()) {
            Type type = deserializerTypesIterator.next();
            Object object = deserializers.get(type);
            builder.registerTypeAdapter(type, object);
        }
        return builder;
    }
}

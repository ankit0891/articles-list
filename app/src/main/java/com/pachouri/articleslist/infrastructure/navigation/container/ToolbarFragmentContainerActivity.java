package com.pachouri.articleslist.infrastructure.navigation.container;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;

import com.pachouri.articleslist.R;

import butterknife.BindView;

/**
 * Activity which extends the Parent basic activity to set the toolbar
 * <p>
 * Created by Ankit on 11/14/18.
 */
public class ToolbarFragmentContainerActivity extends BaseFragmentContainerActivity {

    @BindView(R.id.toolbar)
    protected Toolbar mToolbar;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_base_container;
    }

    @Override
    protected void setToolbar() {
        if (hasToolbar()) {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(shouldShowBackButton());
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            String title = null;
            if (null != mSettings && mSettings.getPageTitle() != 0) {
                title = getString(mSettings.getPageTitle());
            }
            setTitle(title);
        }
    }

    private void setTitle(String title) {
        if (null != mToolbar) {
            AppCompatTextView titleTextView = mToolbar.findViewById(R.id.textViewToolbarTitle);
            if (null != titleTextView) {
                titleTextView.setText(title);
            }
        }
    }
}

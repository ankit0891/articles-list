package com.pachouri.articleslist.infrastructure.api.response.articles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WebShopUrl {

    @SerializedName("href")
    @Expose
    private Object href;

    public Object getHref() {
        return href;
    }
}
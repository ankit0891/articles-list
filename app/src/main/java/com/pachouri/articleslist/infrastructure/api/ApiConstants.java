package com.pachouri.articleslist.infrastructure.api;

import com.pachouri.articleslist.BuildConfig;

/**
 * Add all the constants related with apis
 * <p>
 * Created by Ankit on 11/14/18.
 */
public class ApiConstants {

    public static final String BASE_URL = BuildConfig.BASE_URL;
    public static final String API_PATH = "api/v1/";

    public static final String ENDPOINT_ARTICLES = API_PATH + "articles";
}

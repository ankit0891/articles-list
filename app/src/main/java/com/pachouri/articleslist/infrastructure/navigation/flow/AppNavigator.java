package com.pachouri.articleslist.infrastructure.navigation.flow;

import android.content.Context;
import android.content.Intent;

import com.pachouri.articleslist.infrastructure.api.response.articles.Article_;

import java.util.List;

/**
 * Created by Ankit on 11/14/18.
 */
public interface AppNavigator {

    /**
     * Get the intent to open the Selection Screen
     *
     * @param context
     * @return
     */
    Intent getSelectionScreenIntent(Context context);

    /**
     * Get the intent to open the Review Screen
     *
     * @param context
     * @param list
     * @return
     */
    Intent getReviewScreenIntent(Context context, List<Article_> list);
}

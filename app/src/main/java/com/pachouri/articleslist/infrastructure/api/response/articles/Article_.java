package com.pachouri.articleslist.infrastructure.api.response.articles;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Article_ implements Parcelable {

    @SerializedName("description")
    @Expose
    private Object description;
    @SerializedName("prevPrice")
    @Expose
    private Object prevPrice;
    @SerializedName("manufacturePrice")
    @Expose
    private Object manufacturePrice;
    @SerializedName("delivery")
    @Expose
    private Delivery delivery;
    @SerializedName("brand")
    @Expose
    private Brand brand;
    @SerializedName("media")
    @Expose
    private List<Medium> media = null;
    @SerializedName("assemblyService")
    @Expose
    private Object assemblyService;
    @SerializedName("availability")
    @Expose
    private Object availability;
    @SerializedName("url")
    @Expose
    private Object url;
    @SerializedName("energyClass")
    @Expose
    private Object energyClass;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("price")
    @Expose
    private Price price;
    @SerializedName("_metadata")
    @Expose
    private Metadata__ metadata;
    @SerializedName("_links")
    @Expose
    private Links_ links;

    private boolean isLiked;

    protected Article_(Parcel in) {
        sku = in.readString();
        title = in.readString();
        media = in.readArrayList(Medium.class.getClassLoader());
        isLiked = (in.readInt() == 1) ? true : false;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(sku);
        parcel.writeString(title);
        parcel.writeList(media);
        parcel.writeInt(isLiked ? 1 : 0);
    }

    public static final Creator<Article_> CREATOR = new Creator<Article_>() {
        @Override
        public Article_ createFromParcel(Parcel in) {
            return new Article_(in);
        }

        @Override
        public Article_[] newArray(int size) {
            return new Article_[size];
        }
    };

    public Object getDescription() {
        return description;
    }

    public Object getPrevPrice() {
        return prevPrice;
    }

    public Object getManufacturePrice() {
        return manufacturePrice;
    }

    public Delivery getDelivery() {
        return delivery;
    }

    public Brand getBrand() {
        return brand;
    }

    public List<Medium> getMedia() {
        return media;
    }

    public Object getAssemblyService() {
        return assemblyService;
    }

    public Object getAvailability() {
        return availability;
    }

    public Object getUrl() {
        return url;
    }

    public Object getEnergyClass() {
        return energyClass;
    }

    public String getSku() {
        return sku;
    }

    public String getTitle() {
        return title;
    }

    public Price getPrice() {
        return price;
    }

    public Metadata__ getMetadata() {
        return metadata;
    }

    public Links_ getLinks() {
        return links;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public boolean isLiked() {
        return isLiked;
    }

    public void setLiked(boolean liked) {
        isLiked = liked;
    }
}
package com.pachouri.articleslist.infrastructure.api.response.articles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Brand {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("logo")
    @Expose
    private List<String> logo = null;
    @SerializedName("description")
    @Expose
    private String description;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getLogo() {
        return logo;
    }

    public String getDescription() {
        return description;
    }
}

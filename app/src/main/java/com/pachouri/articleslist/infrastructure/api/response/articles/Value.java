package com.pachouri.articleslist.infrastructure.api.response.articles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Value {

    @SerializedName("colorCode")
    @Expose
    private String colorCode;
    @SerializedName("colorImage")
    @Expose
    private String colorImage;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("_metadata")
    @Expose
    private Metadata metadata;

    public String getColorCode() {
        return colorCode;
    }

    public String getColorImage() {
        return colorImage;
    }

    public String getId() {
        return id;
    }

    public Integer getPriority() {
        return priority;
    }

    public String getTitle() {
        return title;
    }

    public Metadata getMetadata() {
        return metadata;
    }
}
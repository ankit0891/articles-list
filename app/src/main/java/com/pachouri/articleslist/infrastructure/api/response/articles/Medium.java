package com.pachouri.articleslist.infrastructure.api.response.articles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Medium implements Serializable{

    @SerializedName("uri")
    @Expose
    private String uri;
    @SerializedName("mimeType")
    @Expose
    private String mimeType;
    @SerializedName("type")
    @Expose
    private Object type;
    @SerializedName("priority")
    @Expose
    private Integer priority;
    @SerializedName("size")
    @Expose
    private Object size;

    public String getUri() {
        return uri;
    }

    public String getMimeType() {
        return mimeType;
    }

    public Object getType() {
        return type;
    }

    public Integer getPriority() {
        return priority;
    }

    public Object getSize() {
        return size;
    }
}
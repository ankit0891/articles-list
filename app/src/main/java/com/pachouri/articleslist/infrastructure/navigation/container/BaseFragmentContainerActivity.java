package com.pachouri.articleslist.infrastructure.navigation.container;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.pachouri.articleslist.R;
import com.pachouri.articleslist.infrastructure.navigation.flow.FlowRoutes;

import java.security.InvalidParameterException;

import butterknife.ButterKnife;

/**
 * This is the basic activity which has all the basic utilities needed throughout the app
 * <p>
 * Created by Ankit on 11/14/18.
 */
public abstract class BaseFragmentContainerActivity extends AppCompatActivity {

    public static final String BUNDLE_ROUTE = "container.route";

    protected abstract @LayoutRes
    int getLayoutId();

    protected abstract void setToolbar();

    protected FlowRoutes mRoute;
    protected Fragment mFragment;
    protected FragmentContainerSettings mSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getIntent().getExtras();
        String routeName = bundle.getString(BUNDLE_ROUTE);
        if (null == routeName) {
            throw new InvalidParameterException("You need to pass a FlowRoutes name.");
        }
        mRoute = FlowRoutes.valueOf(routeName);
        if (null == mRoute) {
            throw new InvalidParameterException("Please pass a valid Route");
        }
        mFragment = getFragment();
        if (!(mFragment instanceof FragmentContainerSettings)) {
            throw new RuntimeException("Fragment needs to implement FragmentContainerSettings!");
        }
        mSettings = (FragmentContainerSettings) mFragment;

        setContentView(getLayoutId());
        ButterKnife.bind(this);
        attachNewFragment(mFragment, false);
        setToolbar();
    }

    protected Fragment getFragment() {
        Fragment fragment = mRoute.getFragmentInstance();
        if (null == fragment) {
            throw new InvalidParameterException("This route has no fragment! So either override getFragment or use a proper route");
        }
        return fragment;
    }

    private void attachNewFragment(Fragment fragment, Boolean backStack) {
        fragment.setArguments(getIntent().getExtras());
        String TAG = fragment.getClass().getName();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentContainer, fragment);
        if (backStack) {
            fragmentTransaction.addToBackStack(TAG);
        }
        fragmentTransaction.commit();
    }

    protected boolean shouldShowBackButton() {
        return null != mSettings && mSettings.shouldShowBackButton();
    }

    protected boolean hasToolbar() {
        return null != mSettings && mSettings.hasToolbar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (null != mSettings && mSettings.onBackPressed()) {
            super.onBackPressed();
            overridePendingTransition(R.anim.enter_screen, R.anim.exit_screen);
        }
    }
}

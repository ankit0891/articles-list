package com.pachouri.articleslist.infrastructure.api.response.articles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Self_ {

    @SerializedName("href")
    @Expose
    private String href;

    public String getHref() {
        return href;
    }
}
package com.pachouri.articleslist.infrastructure.api;

import android.arch.lifecycle.LifecycleObserver;

import com.pachouri.articleslist.infrastructure.api.callback.Callback;
import com.pachouri.articleslist.infrastructure.api.request.query.ListQuery;
import com.pachouri.articleslist.infrastructure.api.response.BaseResponse;

import io.reactivex.disposables.Disposable;

/**
 * Created by Ankit on 11/14/18.
 */
public interface AppApi extends LifecycleObserver, Disposable {

    /**
     * Get the Articles list
     *
     * @param query
     * @param callback
     * @return
     */
    Disposable getArticlesList(ListQuery query, Callback<BaseResponse> callback);
}

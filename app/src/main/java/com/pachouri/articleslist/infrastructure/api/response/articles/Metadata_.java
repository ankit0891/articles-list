package com.pachouri.articleslist.infrastructure.api.response.articles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Metadata_ {

    @SerializedName("type")
    @Expose
    private String type;

    public String getType() {
        return type;
    }
}
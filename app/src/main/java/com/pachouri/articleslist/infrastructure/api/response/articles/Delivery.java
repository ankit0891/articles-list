package com.pachouri.articleslist.infrastructure.api.response.articles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Delivery {

    @SerializedName("time")
    @Expose
    private Time time;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("terms")
    @Expose
    private Object terms;
    @SerializedName("deliveredBy")
    @Expose
    private Object deliveredBy;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("typeLabelLink")
    @Expose
    private String typeLabelLink;

    public Time getTime() {
        return time;
    }

    public String getType() {
        return type;
    }

    public Object getTerms() {
        return terms;
    }

    public Object getDeliveredBy() {
        return deliveredBy;
    }

    public String getText() {
        return text;
    }

    public String getTypeLabelLink() {
        return typeLabelLink;
    }
}
package com.pachouri.articleslist.infrastructure.api.request.query;

import android.support.annotation.Nullable;

import java.security.InvalidParameterException;
import java.util.Map;

/**
 * Created by Ankit on 11/14/18.
 */
public interface BaseListQuery {

    Map<String, String> getQueryMap() throws InvalidParameterException;

    String getAppDomain();

    String getLocale();

    Integer getOffset();

    Integer getLimit();

    ListQuery setAppDomain(String appDomain);

    ListQuery setLocale(String locale);

    ListQuery setOffset(@Nullable int offset);

    ListQuery setLimit(int limit);
}

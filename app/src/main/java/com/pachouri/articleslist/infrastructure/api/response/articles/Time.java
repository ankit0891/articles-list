package com.pachouri.articleslist.infrastructure.api.response.articles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Time {

    @SerializedName("renderAs")
    @Expose
    private String renderAs;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("units")
    @Expose
    private String units;

    public String getRenderAs() {
        return renderAs;
    }

    public String getAmount() {
        return amount;
    }

    public String getUnits() {
        return units;
    }
}
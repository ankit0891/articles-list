package com.pachouri.articleslist.infrastructure.navigation.container;

import android.support.annotation.StringRes;

/**
 * Interface to implement the basic methods
 * <p>
 * Created by Ankit on 11/14/18.
 */
public interface FragmentContainerSettings {

    /**
     * Should the activity has onBackPressed enabled
     * <p>
     * Return true if you want the activity to handle on back pressed
     *
     * @return
     */
    boolean onBackPressed();

    /**
     * Should the toolbar show the back button
     * <p>
     * Return true if you want the activity to have back arrow on top
     *
     * @return
     */
    boolean shouldShowBackButton();

    /**
     * Should the current screen have toolbar
     * <p>
     * Return true if you want the activity to have the toolbar
     *
     * @return
     */
    boolean hasToolbar();

    /**
     * Get the page title.
     *
     * @return
     */
    @StringRes
    int getPageTitle();
}

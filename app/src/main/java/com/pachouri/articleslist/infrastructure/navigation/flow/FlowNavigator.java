package com.pachouri.articleslist.infrastructure.navigation.flow;

import android.content.Context;
import android.content.Intent;

import com.pachouri.articleslist.infrastructure.api.response.articles.Article_;
import com.pachouri.articleslist.infrastructure.navigation.container.ToolbarFragmentContainerActivity;
import com.pachouri.articleslist.main.fragment.ReviewScreenFragment;
import com.pachouri.articleslist.util.AppConstants;

import java.util.List;

/**
 * Created by Ankit on 11/14/18.
 */
public class FlowNavigator implements AppNavigator {

    /**
     * Get the intent to open the Selection Screen
     *
     * @param context
     * @return
     */
    @Override
    public Intent getSelectionScreenIntent(Context context) {
        Intent intent = FlowRoutes.SelectionScreen.getIntent(context, ToolbarFragmentContainerActivity.class);
        return intent;
    }

    /**
     * Get the intent to open the Review Screen
     *
     * @param context
     * @param list
     * @return
     */
    @Override
    public Intent getReviewScreenIntent(Context context, List<Article_> list) {
        Intent intent = FlowRoutes.ReviewScreen.getIntent(context, ToolbarFragmentContainerActivity.class);
        intent.putExtra(AppConstants.BUNDLE_ARTICLES_LIST, ReviewScreenFragment.createBundle(list));
        return intent;
    }
}

package com.pachouri.articleslist.infrastructure.api.provider.retrofit2;

import android.arch.lifecycle.OnLifecycleEvent;
import android.content.Context;

import com.pachouri.articleslist.infrastructure.api.AppApi;
import com.pachouri.articleslist.infrastructure.api.callback.Callback;
import com.pachouri.articleslist.infrastructure.api.observer.RetrofitCallbackObserver;
import com.pachouri.articleslist.infrastructure.api.request.query.ListQuery;
import com.pachouri.articleslist.infrastructure.api.response.BaseResponse;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.arch.lifecycle.Lifecycle.Event.ON_DESTROY;

/**
 * Created by Ankit on 11/14/18.
 */
public class RetrofitApi implements AppApi {

    private Context mContext;
    private CompositeDisposable mCompositeDisposable;

    public RetrofitApi(Context context) {
        mContext = context;
        mCompositeDisposable = new CompositeDisposable();
    }

    @Override
    @OnLifecycleEvent(ON_DESTROY)
    public void dispose() {
        mCompositeDisposable.dispose();
    }

    @Override
    public boolean isDisposed() {
        return mCompositeDisposable.isDisposed();
    }

    /**
     * Api to get the articles list
     *
     * @param query
     * @param callback
     * @return
     */
    @Override
    public Disposable getArticlesList(ListQuery query, Callback<BaseResponse> callback) {
        Disposable disposable = RetrofitSingleton.getInstance(mContext).getArticlesList(query.getQueryMap())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new RetrofitCallbackObserver<>(callback));
        mCompositeDisposable.add(disposable);
        return disposable;
    }
}

package com.pachouri.articleslist.infrastructure.api.callback;

/**
 * Callback for the api calls
 * <p>
 * Created by Ankit on 11/14/18.
 */
public abstract class Callback<T> {

    public abstract void onSuccess(T response);

    public abstract void onError(String error);

    public void onStart() {
    }

    public void onComplete() {
    }
}

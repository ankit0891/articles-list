package com.pachouri.articleslist.infrastructure.api.response.articles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Embedded<T> implements Serializable{

    @SerializedName("filters")
    @Expose
    private List<Filter> filters = null;
    @SerializedName("articles")
    @Expose
    private List<T> articles = null;

    public List<Filter> getFilters() {
        return filters;
    }

    public List<T> getArticles() {
        return articles;
    }
}
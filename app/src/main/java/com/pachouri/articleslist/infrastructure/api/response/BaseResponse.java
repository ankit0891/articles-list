package com.pachouri.articleslist.infrastructure.api.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pachouri.articleslist.infrastructure.api.response.articles.Embedded;
import com.pachouri.articleslist.infrastructure.api.response.articles.Links;

public class BaseResponse {

    @SerializedName("resourceType")
    @Expose
    private String resourceType;
    @SerializedName("articlesCount")
    @Expose
    private Integer articlesCount;
    @SerializedName("_links")
    @Expose
    private Links links;
    @SerializedName("_embedded")
    @Expose
    private Embedded embedded;

    public String getResourceType() {
        return resourceType;
    }

    public Integer getArticlesCount() {
        return articlesCount;
    }

    public Links getLinks() {
        return links;
    }

    public Embedded getEmbedded() {
        return embedded;
    }
}


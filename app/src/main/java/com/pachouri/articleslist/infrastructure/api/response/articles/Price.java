package com.pachouri.articleslist.infrastructure.api.response.articles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Price {

    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("currency")
    @Expose
    private String currency;
    @SerializedName("isRecommendedRetailPrice")
    @Expose
    private Boolean isRecommendedRetailPrice;

    public String getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public Boolean getRecommendedRetailPrice() {
        return isRecommendedRetailPrice;
    }
}
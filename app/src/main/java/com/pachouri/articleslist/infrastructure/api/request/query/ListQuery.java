package com.pachouri.articleslist.infrastructure.api.request.query;

import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.security.InvalidParameterException;
import java.util.HashMap;
import java.util.Map;

/**
 * Class to create the query map
 * <p>
 * Created by Ankit on 11/14/18.
 */
public class ListQuery implements BaseListQuery {

    private String mAppDomain;
    private String mLocale;
    private Integer mOffset;
    private Integer mLimit;

    public ListQuery() {
        mAppDomain = "1";
        mLocale = "de_DE";
        mLimit = 10;
    }

    @Override
    public Map<String, String> getQueryMap() throws InvalidParameterException {
        Map<String, String> map = new HashMap<>();

        if (!TextUtils.isEmpty(mAppDomain)) {
            map.put("appDomain", mAppDomain);
        }
        if (!TextUtils.isEmpty(mLocale)) {
            map.put("locale", mLocale);
        }
        if (mOffset != null) {
            map.put("offset", mOffset.toString());
        }
        if (mLimit != null) {
            map.put("limit", mLimit.toString());
        }
        return map;
    }

    @Override
    public String getAppDomain() {
        return mAppDomain;
    }

    @Override
    public String getLocale() {
        return mLocale;
    }

    @Override
    public Integer getOffset() {
        return mOffset;
    }

    @Override
    public Integer getLimit() {
        return mLimit;
    }

    @Override
    public ListQuery setAppDomain(String appDomain) {
        mAppDomain = appDomain;
        return this;
    }

    @Override
    public ListQuery setLocale(String locale) {
        mLocale = locale;
        return this;
    }

    @Override
    public ListQuery setOffset(@Nullable int offset) {
        mOffset = new Integer(offset);
        return this;
    }

    @Override
    public ListQuery setLimit(int limit) {
        mLimit = new Integer(limit);
        return this;
    }
}

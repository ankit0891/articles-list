package com.pachouri.articleslist.infrastructure.api.response.articles;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Links_ {

    @SerializedName("self")
    @Expose
    private Self_ self;
    @SerializedName("webShopUrl")
    @Expose
    private WebShopUrl webShopUrl;

    public Self_ getSelf() {
        return self;
    }

    public WebShopUrl getWebShopUrl() {
        return webShopUrl;
    }
}
package com.pachouri.articleslist.infrastructure.api.observer;

import com.pachouri.articleslist.infrastructure.api.callback.Callback;

import io.reactivex.observers.DisposableObserver;

/**
 * Observer for the Api callback
 * <p>
 * Created by Ankit on 11/14/18.
 */
public class RetrofitCallbackObserver<T> extends DisposableObserver<T> {

    private Callback<T> mCallback;

    public RetrofitCallbackObserver(Callback<T> callback) {
        mCallback = callback;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mCallback != null) {
            mCallback.onStart();
        }
    }

    @Override
    public void onComplete() {
        if (mCallback != null) {
            mCallback.onComplete();
        }
    }

    @Override
    public void onNext(T t) {
        if (t != null) {
            onSuccess(t);
        } else {
            onError("Unknown Error");
        }
    }

    @Override
    public void onError(Throwable e) {
        onError(e.getMessage());
    }

    private void onError(String error) {
        if (mCallback != null) {
            mCallback.onError(error);
        }
    }

    private void onSuccess(T response) {
        if (mCallback != null) {
            mCallback.onSuccess(response);
        }
    }
}

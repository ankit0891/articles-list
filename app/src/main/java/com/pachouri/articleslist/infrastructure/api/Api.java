package com.pachouri.articleslist.infrastructure.api;

import android.app.Application;
import android.arch.lifecycle.LifecycleOwner;

import com.pachouri.articleslist.infrastructure.api.provider.retrofit2.RetrofitApi;

/**
 * Created by Ankit on 11/14/18.
 */

/**
 * Class to get the RetrofitApi instance,
 * The purpose of this class is - in future if we wish to change the provider
 * we can simply change it from here, we do not need to change all the api
 * calls throughout the app.
 */
public class Api {

    public static AppApi getInstance(Application application, LifecycleOwner owner) {
        AppApi api = new RetrofitApi(application);
        owner.getLifecycle().addObserver(api);
        return api;
    }
}

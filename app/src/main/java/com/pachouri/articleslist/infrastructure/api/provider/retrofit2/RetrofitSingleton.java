package com.pachouri.articleslist.infrastructure.api.provider.retrofit2;

import android.content.Context;

import com.pachouri.articleslist.infrastructure.api.ApiConstants;
import com.pachouri.articleslist.infrastructure.api.GsonProvider;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Retrofit setup and get its singleton
 * <p>
 * Created by Ankit on 11/14/18.
 */
public class RetrofitSingleton {

    private static RetrofitInterface mInstance;

    public static synchronized RetrofitInterface getInstance(Context context) {

        if (mInstance == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient okHttpClient = new OkHttpClient()
                    .newBuilder()
                    .addInterceptor(logging)
                    .connectTimeout(120, TimeUnit.SECONDS)
                    .readTimeout(120, TimeUnit.SECONDS)
                    .writeTimeout(120, TimeUnit.SECONDS)
                    .build();

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ApiConstants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(GsonProvider.getSingletonGson()))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(okHttpClient)
                    .build();

            mInstance = retrofit.create(RetrofitInterface.class);
        }
        return mInstance;
    }
}
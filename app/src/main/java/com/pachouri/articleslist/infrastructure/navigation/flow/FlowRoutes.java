package com.pachouri.articleslist.infrastructure.navigation.flow;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import com.pachouri.articleslist.infrastructure.navigation.container.BaseFragmentContainerActivity;
import com.pachouri.articleslist.main.fragment.ReviewScreenFragment;
import com.pachouri.articleslist.main.fragment.SelectionScreenFragment;

import java.lang.reflect.InvocationTargetException;

/**
 * All the fragments are declared here so that all the intents will open through a single class
 * <p>
 * Created by Ankit on 11/14/18.
 */
public enum FlowRoutes {

    SelectionScreen(SelectionScreenFragment.class),
    ReviewScreen(ReviewScreenFragment.class);

    private Class<? extends Fragment> fragmentClass;

    FlowRoutes(Class<? extends Fragment> fragmentClass) {
        this.fragmentClass = fragmentClass;
    }

    public Intent getIntent(Context activity, Class<? extends Activity> activityClass) {
        Intent intent = new Intent(activity, activityClass);
        intent.putExtra(BaseFragmentContainerActivity.BUNDLE_ROUTE, name());
        return intent;
    }

    public Fragment getFragmentInstance() {
        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.getConstructors()[0].newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
        return fragment;
    }
}

package com.pachouri.articleslist.util;

/**
 * Purpose of this class is to keep all the constants at a single place
 * <p>
 * Created by Ankit on 11/17/18.
 */
public class AppConstants {

    public static final String INTENT_ARTICLES_LIST = "INTENT_ARTICLES_LIST";
    public static final String BUNDLE_ARTICLES_LIST = "BUNDLE_ARTICLES_LIST";
}

package com.pachouri.articleslist.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.widget.Toast;

/**
 * Add the util methods, variables etc. in this class to be used throughout the app
 * <p>
 * Created by Ankit on 11/14/18.
 */
public class CommonUtils {

    /**
     * Method to show toast
     *
     * @param context
     * @param message
     */
    public static void showToast(Context context, String message) {
        if (null != context) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Method to check internet connection available or not
     *
     * @param context
     * @return
     */
    public static boolean isInternetConnected(Context context) {
        boolean isInternetConnected = false;
        if (null != context) {
            try {
                ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context
                        .CONNECTIVITY_SERVICE);
                if (connManager.getActiveNetworkInfo() != null && connManager.getActiveNetworkInfo()
                        .isAvailable() && connManager.getActiveNetworkInfo().isConnected()) {
                    isInternetConnected = true;
                }
            } catch (Exception ex) {
                isInternetConnected = false;
            }
        }
        return isInternetConnected;
    }

    /**
     * Method to convert dp to pixel
     *
     * @param context
     * @param dp
     * @return
     */
    public static float convertDpToPixel(Context context, float dp) {
        float density = context.getResources().getDisplayMetrics().density;
        float pixel = dp * density;
        return pixel;
    }
}

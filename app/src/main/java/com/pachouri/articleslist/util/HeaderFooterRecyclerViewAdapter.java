package com.pachouri.articleslist.util;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * RecyclerView Adapter to create different view types in list
 * <p>
 * Created by Ankit on 11/15/18.
 */
public abstract class HeaderFooterRecyclerViewAdapter<H extends RecyclerView.ViewHolder, C extends RecyclerView.ViewHolder, F extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int HEADER_TYPE = 1;
    private static final int CONTENT_TYPE = 2;
    private static final int FOOTER_TYPE = 3;

    protected Context mContext;

    public abstract H onCreateHeaderViewHolder(ViewGroup parent);

    public abstract C onCreateContentViewHolder(ViewGroup parent, int viewType);

    public abstract F onCreateFooterViewHolder(ViewGroup parent);

    public abstract void onBindHeaderViewHolder(H holder, int position);

    public abstract void onBindContentViewHolder(C holder, int position);

    public abstract void onBindFooterViewHolder(F holder, int position);

    public abstract int getHeaderCount();

    public abstract int getContentCount();

    public abstract int getFooterCount();

    public HeaderFooterRecyclerViewAdapter(Context context) {
        mContext = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int type) {
        if (type == HEADER_TYPE) {
            return onCreateHeaderViewHolder(parent);
        } else if (type == FOOTER_TYPE) {
            return onCreateFooterViewHolder(parent);
        }
        return onCreateContentViewHolder(parent, type);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(position);
        if (type == HEADER_TYPE) {
            onBindHeaderViewHolder((H) holder, convertToHeaderIndex(position));
        } else if (type == FOOTER_TYPE) {
            onBindFooterViewHolder((F) holder, convertToFooterIndex(position));
        } else {
            onBindContentViewHolder((C) holder, convertToContentIndex(position));
        }
    }

    /**
     * Get total items count
     *
     * @return
     */
    @Override
    public int getItemCount() {
        return getContentCount() + getHeaderCount() + getFooterCount();
    }

    /**
     * Get the itemViewType on the basis of position and header/footer counts
     *
     * @param position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        int offset = 0;
        if (getHeaderCount() > 0) {
            offset = offset + getHeaderCount();
            if (position < getHeaderCount()) {
                return HEADER_TYPE;
            }
        }
        if (getFooterCount() > 0) {
            if (position >= getContentCount() + offset) {
                return FOOTER_TYPE;
            }
        }
        return CONTENT_TYPE;
    }

    /**
     * Get the actual position of the content
     *
     * @param adapterPosition
     * @return
     */
    protected int convertToContentIndex(int adapterPosition) {
        return adapterPosition - getHeaderCount();
    }

    /**
     * Get the position of the header
     *
     * @param adapterPosition
     * @return
     */
    protected int convertToHeaderIndex(int adapterPosition) {
        return adapterPosition;
    }

    /**
     * Get the position of the footer
     *
     * @param adapterPosition
     * @return
     */
    protected int convertToFooterIndex(int adapterPosition) {
        return adapterPosition - getHeaderCount() - getContentCount();
    }
}

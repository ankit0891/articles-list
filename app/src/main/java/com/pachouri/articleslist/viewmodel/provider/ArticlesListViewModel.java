package com.pachouri.articleslist.viewmodel.provider;

import android.app.Application;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.pachouri.articleslist.infrastructure.api.Api;
import com.pachouri.articleslist.infrastructure.api.AppApi;
import com.pachouri.articleslist.infrastructure.api.callback.Callback;
import com.pachouri.articleslist.infrastructure.api.request.query.ListQuery;
import com.pachouri.articleslist.infrastructure.api.response.BaseResponse;
import com.pachouri.articleslist.infrastructure.api.response.articles.Article_;
import com.pachouri.articleslist.viewmodel.model.LoadingState;
import com.pachouri.articleslist.viewmodel.model.MutableLiveList;

/**
 * Created by Ankit on 11/14/18.
 */
public class ArticlesListViewModel extends BaseListViewModel<BaseResponse, Article_, ListQuery> {

    private ListQuery mQuery;
    private MutableLiveList<Article_> mList;
    private AppApi mApi;

    public ArticlesListViewModel(@NonNull Application application) {
        super(application);
        mQuery = new ListQuery();
        mApi = Api.getInstance(application, this);
    }

    @Override
    protected void makeListApiCall(MutableLiveData<LoadingState> state, Callback<BaseResponse> callback) {
        mApi.getArticlesList(mQuery, callback);
    }

    @Override
    protected ListQuery getQuery() {
        return mQuery;
    }

    @Override
    protected MutableLiveList<Article_> initialiseList() {
        if (null == mList) {
            mList = new MutableLiveList<>();
        }
        return mList;
    }
}

package com.pachouri.articleslist.viewmodel.provider;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;

/**
 * The purpose of this class is to use the View Model and make it lifecycle friendly
 * <p>
 * Created by Ankit on 11/14/18.
 */
public class AppViewModel extends AndroidViewModel implements LifecycleOwner {

    private LifecycleRegistry mRegister;

    public AppViewModel(@NonNull Application application) {
        super(application);
        mRegister = new LifecycleRegistry(this);
        mRegister.markState(Lifecycle.State.INITIALIZED);
        mRegister.markState(Lifecycle.State.CREATED);
        mRegister.markState(Lifecycle.State.STARTED);
    }

    @Override
    @CallSuper
    public void onCleared() {
        mRegister.markState(Lifecycle.State.DESTROYED);
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return mRegister;
    }
}

package com.pachouri.articleslist.viewmodel.provider;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;

import com.pachouri.articleslist.infrastructure.api.callback.Callback;
import com.pachouri.articleslist.infrastructure.api.request.query.BaseListQuery;
import com.pachouri.articleslist.infrastructure.api.response.BaseResponse;
import com.pachouri.articleslist.viewmodel.BaseListViewModelContract;
import com.pachouri.articleslist.viewmodel.model.LoadingState;
import com.pachouri.articleslist.viewmodel.model.MutableLiveList;

import java.util.List;

/**
 * This View Model should be reusable for all the types of list
 * <p>
 * Created by Ankit on 11/14/18.
 */
public abstract class BaseListViewModel<T, A, Q extends BaseListQuery> extends AppViewModel implements BaseListViewModelContract<A> {

    /**
     * List wrapped in a MutableLiveList.
     */
    private final MutableLiveList<A> mList;

    /**
     * Load State for the initial load
     */
    private final MutableLiveData<LoadingState> mInitialLoadState;

    /**
     * Load state for on scroll (load more)
     */
    private final MutableLiveData<LoadingState> mLoadMoreLoadState;

    /**
     * Does this list have more items
     */
    private final MutableLiveData<Boolean> mCanLoadMoreItems;

    /**
     * Get the total count of the items coming in the response
     */
    private final MutableLiveData<Integer> mTotalCount;

    /**
     * Abstract method to make the api call
     *
     * @param state
     * @param callback
     */
    protected abstract void makeListApiCall(MutableLiveData<LoadingState> state, Callback<T> callback);

    /**
     * Get the query
     *
     * @return
     */
    protected abstract Q getQuery();

    /**
     * @return
     */
    protected abstract MutableLiveList<A> initialiseList();


    public BaseListViewModel(@NonNull Application application) {
        super(application);
        mList = initialiseList();
        mInitialLoadState = new MutableLiveData<>();
        mLoadMoreLoadState = new MutableLiveData<>();
        mCanLoadMoreItems = new MutableLiveData<>();
        mCanLoadMoreItems.setValue(true);
        mTotalCount = new MutableLiveData<>();
    }

    @Override
    public LiveData<LoadingState> getInitialLoadState() {
        return mInitialLoadState;
    }

    @Override
    public LiveData<LoadingState> getLoadMoreLoadState() {
        return mLoadMoreLoadState;
    }

    @Override
    public LiveData<List<A>> getList() {
        return mList;
    }

    @Override
    public LiveData<Boolean> getCanLoadMoreItems() {
        return mCanLoadMoreItems;
    }

    @Override
    public LiveData<Integer> getTotalCount() {
        return mTotalCount;
    }

    @Override
    public boolean loadMore() {
        if (null != mCanLoadMoreItems.getValue() && mCanLoadMoreItems.getValue()) {
            return loadItems(mLoadMoreLoadState);
        }
        return false;
    }

    @Override
    public boolean refresh() {
        if (mInitialLoadState.getValue() != LoadingState.LOADING_COMPLETED) {
            return initialRefresh();
        }

        return false;
    }


    @Override
    public void onCleared() {
        super.onCleared();
    }

    /**
     * Handles the initial refresh should only be called when the first page load hasn't been called successfully
     *
     * @return true if the api call was made or was already in progress, false otherwise.
     */
    private boolean initialRefresh() {
        return loadItems(mInitialLoadState);
    }


    private boolean loadItems(final MutableLiveData<LoadingState> state) {
        //Make sure nothing is loading.
        if (mLoadMoreLoadState.getValue() != LoadingState.LOADING
                && mInitialLoadState.getValue() != LoadingState.LOADING) {
            Log.v("LIST_LOAD OFFSET", ": " + mList.size());
            getQuery().setOffset(mList.size());
            //set the current state to load to prevent a lock.
            state.setValue(LoadingState.LOADING);
            makeListApiCall(state, new Callback<T>() {
                @Override
                public void onSuccess(T response) {
                    if (null != response && response instanceof BaseResponse) {
                        BaseResponse baseResponse = (BaseResponse) response;
                        mTotalCount.setValue(baseResponse.getArticlesCount());
                        mList.addAll(baseResponse.getEmbedded().getArticles());
                        Log.v("LIST_LOAD", "ITEMS SIZE: " + baseResponse.getEmbedded().getArticles().size());
                        state.setValue(LoadingState.LOADING_COMPLETED);
                        boolean noMoreItems = !(baseResponse.getEmbedded().getArticles().size() < getQuery().getLimit());

                        /**
                         *  To test no more items remaining, uncomment the below set of code
                         */

                        /*if (mList.size() > 30) {
                            noMoreItems = false;
                        }*/

                        if (noMoreItems != mCanLoadMoreItems.getValue()) {
                            mCanLoadMoreItems.setValue(noMoreItems);
                            if (noMoreItems) {
                                Log.v("LIST_LOAD", "NO MORE ITEMS");
                            }
                        }
                    } else {
                        state.setValue(LoadingState.LOADING_COMPLETED);
                    }
                }

                @Override
                public void onError(String error) {
                    state.setValue(LoadingState.LOADING_FAILED.withError(error));
                }
            });
            return true;
        } else//If loading doesn't happen cause of other loading, mark the state
        {
            return state.getValue() == LoadingState.LOADING;
        }
    }
}

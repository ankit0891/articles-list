package com.pachouri.articleslist.viewmodel.model;

/**
 * To get the Loading states
 * <p>
 * Created by Ankit on 11/14/18.
 */
public enum LoadingState {

    LOADING,
    LOADING_COMPLETED,
    LOADING_FAILED;

    private String mError;

    LoadingState() {
        mError = null;
    }

    public String getError() {
        return mError;
    }

    public LoadingState withError(String error) {
        mError = error;
        return this;
    }
}

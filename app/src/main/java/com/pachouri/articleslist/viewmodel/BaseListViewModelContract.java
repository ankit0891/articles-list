package com.pachouri.articleslist.viewmodel;

import android.arch.lifecycle.LiveData;

import com.pachouri.articleslist.viewmodel.model.LoadingState;

import java.util.List;

/**
 * Interface to get the different states needed when a list is loaded
 * <p>
 * Created by Ankit on 11/14/18.
 */
public interface BaseListViewModelContract<T> {

    /**
     * Get the load state for the initial page load.
     *
     * @return
     */
    LiveData<LoadingState> getInitialLoadState();

    /**
     * Get an observable wrapper for the load state for load more.
     *
     * @return
     */
    LiveData<LoadingState> getLoadMoreLoadState();

    /**
     * Get an observable wrapper for the list of objects currently loaded.
     *
     * @return
     */
    LiveData<List<T>> getList();

    /**
     * Get an observable wrapper for has more
     *
     * @return
     */
    LiveData<Boolean> getCanLoadMoreItems();

    /**
     * Get an observable wrapper for total count
     * @return
     */
    LiveData<Integer> getTotalCount();

    /**
     * Triggers a load more - call this when the user scrolls down.
     *
     * @return
     */
    boolean loadMore();

    /**
     * Triggers loading new items when the page first loads.
     *
     * @return
     */
    boolean refresh();
}

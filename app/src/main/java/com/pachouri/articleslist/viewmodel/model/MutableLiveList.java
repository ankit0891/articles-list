package com.pachouri.articleslist.viewmodel.model;

import android.arch.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

/**
 * Extends Live Data class to hold a live list data
 * <p>
 * Created by Ankit on 11/14/18.
 */
public class MutableLiveList<T> extends MutableLiveData<List<T>> {

    /**
     * Adds all the items to the list.
     *
     * @param list
     */
    public void addAll(List<T> list) {
        if (null != list && !list.isEmpty()) {
            List<T> oldList = this.getCurrentNonNullValue();
            oldList.addAll(list);
            this.setValue(oldList);
        }
    }

    /**
     * Get the size of the list
     *
     * @return
     */
    public int size() {
        if (null != getValue()) {
            return getValue().size();
        }
        return 0;
    }

    /**
     * Gets the current value and if its null returns a new value.
     *
     * @return
     */
    private List<T> getCurrentNonNullValue() {
        List<T> oldList = this.getValue();
        if (null == oldList) {
            oldList = new ArrayList<>();
        }
        return oldList;
    }
}

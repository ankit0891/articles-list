package com.pachouri.articleslist.viewmodel.factory;

import android.app.Application;
import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import com.pachouri.articleslist.viewmodel.provider.ArticlesListViewModel;

/**
 * Created by Ankit on 11/14/18.
 */
public class AppViewModelFactory extends ViewModelProvider.NewInstanceFactory {

    private Application mApplication;

    private AppViewModelFactory(Fragment fragment) {
        mApplication = fragment.getActivity().getApplication();
    }

    public static AppViewModelFactory getInstance(Fragment fragment) {
        AppViewModelFactory factory = new AppViewModelFactory(fragment);
        return factory;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(ArticlesListViewModel.class)) {
            return (T) new ArticlesListViewModel(mApplication);
        }
        throw new IllegalArgumentException("Unknown ViewModel class");
    }
}

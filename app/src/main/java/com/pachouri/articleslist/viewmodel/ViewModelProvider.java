package com.pachouri.articleslist.viewmodel;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.Fragment;

import com.pachouri.articleslist.viewmodel.factory.AppViewModelFactory;
import com.pachouri.articleslist.viewmodel.provider.ArticlesListViewModel;

/**
 * Created by Ankit on 11/14/18.
 */
public class ViewModelProvider {

    private static volatile ViewModelProvider mInstance;

    public static synchronized ViewModelProvider getInstance() {
        if (mInstance == null) {
            synchronized (ViewModelProvider.class) {
                if (mInstance == null) {
                    mInstance = new ViewModelProvider();
                }
            }
        }
        return mInstance;
    }

    public ArticlesListViewModel getArticlesListViewModel(Fragment fragment) {
        AppViewModelFactory factory = AppViewModelFactory.getInstance(fragment);
        return ViewModelProviders.of(fragment, factory).get(ArticlesListViewModel.class.getName(), ArticlesListViewModel.class);
    }
}

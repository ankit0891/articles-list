Hi Reader,

First of all thanks for taking time to read this file :)

**Application Name** - Articles List

**Language Used** - Java

**Description** - This is a native android application to review the articles. The api is available here - [Click](https://api-mobile.home24.com/api/v1/articles?appDomain=1&locale=de_DE&limit=10)

Steps to run/install the project:

There are 2 ways to get this project first is by cloning the project and the other is direct download.

1. To clone project:
	* Open terminal and move to the desired directory where you wish to keep the project.
		"For an instance, lets assume you wish to keep the project in your Home folder. So simply type cd Home/ and press enter. You are now in the directory."
   	* Now write the git clone command along with the remote url.
   	   **Command** - git clone https://bitbucket.org/ankit0891/articles-list.git
   	* The project will start cloning. It shows the progress and displays done when its completed.
   	* Check your directory you have your project available.
	
2. If you do not wish to clone the project.
	* Go to this url and click Download Repository - https://bitbucket.org/ankit0891/articles-list/downloads/
	* Go to your downloads folder and unzip the file.

Now we have the project in our respective directory.

3. Open Android Studio.
4. Click on File -> Open
5. Choose the project. 
6. The project will start building and once the gradle build gets finished, you can simply click the Run (Play icon) button on top to run the project.

Some troubleshoots, if the project does not build.

1. Try clean build and build project again.
2. Check the build tools version is installed or not on your machine. 

Some additional information about the project-

1. The project follows the MVVM Architecture
2. To make the network calls, Retrofit2 Library has been used.
3. New features can be easily tweaked in within the project as have kept most of the code as generics and more reusable.
4. The project has 2 Instrumentation tests to test the Start Screen and Selection Screen launch.
5. Some 3rd party libraries have been used, like glide to load images, CardStackView library to show the article images in a card view with swipe feature etc.
6. Some animations are being used. 
	* Any screen opens with an entering animation from left. 
	* Any back navigation gets the exit animation towards right.
	* When the Review button gets active, it comes with a bounce animation.
	* When any article is liked: The Scale animation on a heart image has been implemented.
	* In the Review Screen, the list items get animated when the list is scrolled.
7. SelectionScreenFragment has 2 Constants declared on top - 
	* To change the load more difference.
	* To activate the Review button when the reviewed articles count reaches equal to this constant.


Some assumptions made while doing the test - 

1. The total articles count means the total count coming in the api response.
2. The Review Screen, would have only those Articles which were loaded in the Selection Screen.
3. Pressing back from the Selection Screen, loses the liked articles count.

Hope you would like it. Please feel free to give the suggestions.

Thanks!
Best Regards,

**Ankit Pachouri**

Email - ankitpachouri0891@gmail.com

Github profile - https://github.com/Pachouri-Sikandar

Portfolio - http://ankitpachouri.online/